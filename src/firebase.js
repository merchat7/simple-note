import firebase from 'firebase';

const config = {
    apiKey: "AIzaSyBzaQF4brzGphwdNY7lxuVZlWec9DuAfVw",
    authDomain: "note-basic.firebaseapp.com",
    databaseURL: "https://note-basic.firebaseio.com",
    projectId: "note-basic",
    storageBucket: "note-basic.appspot.com",
    messagingSenderId: "864186781613"
};

firebase.initializeApp(config);

export default firebase;
export const db = firebase.database();
export const auth = firebase.auth();
