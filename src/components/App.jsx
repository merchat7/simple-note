import React, { Component } from 'react';
import { Route, withRouter } from 'react-router-dom';
import { auth } from '../firebase';

import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import { CircularProgress } from 'material-ui/Progress';
import Button from 'material-ui/Button';

import PrivateRoute from './PrivateRoute';
import Main from './Main';
import Login from './Login';
import Signup from './Signup';
import Forgot from './Forgot';
import Profile from './Profile';

const theme = createMuiTheme();

const AppBarItemStyle = {
    marginRight: 10
};

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            authenticated: false,
            currentUser: null,
            atProfile: false};
        }

    componentWillMount() {
        auth.onAuthStateChanged(user => {
        if (user) {
            this.setState({
                authenticated: true,
                currentUser: user,
                loading: false,
                atProfile: false},
                () => { this.props.history.push('/') }
            );
        } else {
            this.setState({
                authenticated: false,
                currentUser: null,
                loading: false,
                atProfile: false
            });
        }
        });
    }

    render () {
        const { authenticated, currentUser, loading, atProfile} = this.state;
        const content = loading ? (
            <div align="center">
                <CircularProgress size={80} thickness={5} />
            </div>
        ) : (
            <div>
                <PrivateRoute
                    exact path="/"
                    component={Main}
                    authenticated={authenticated}
                    />
                <PrivateRoute
                    exact path="/profile"
                    component={Profile}
                    authenticated={authenticated}
                />
                <Route exact path="/login" component={Login} />
                <Route exact path="/signup" component={Signup} />
                <Route exact path="/forgot" component={Forgot} />
            </div>
        );
        return (
            <MuiThemeProvider theme={theme}>
                <div>
                    <AppBar position="static" color="default">
                        <Toolbar>
                            <Typography variant="title" color="inherit" style={AppBarItemStyle}>
                                Simple Note
                            </Typography>
                            { (authenticated && !atProfile) &&
                            <Button variant="raised" color="primary" style={AppBarItemStyle} onClick={() => {
                                this.props.history.push('/profile');
                                this.setState({atProfile: true});
                            }}>Profile</Button>
                            }
                            { (authenticated && atProfile) &&
                            <Button variant="raised" color="primary" style={AppBarItemStyle} onClick={() => {
                                this.props.history.push('/');
                                this.setState({atProfile: false});
                            }}>Home</Button>
                            }
                            { (currentUser !== null && !currentUser.emailVerified) &&
                                <Typography variant="subheading" color="error" style={AppBarItemStyle}>
                                    Please verify your email [{currentUser.email}]
                                </Typography>
                            }
                            { (currentUser !== null && !currentUser.emailVerified) &&
                                <Button variant="raised" color="default" style={AppBarItemStyle} onClick={() => currentUser.sendEmailVerification()
                                .then(function() {alert("Email sent")})
                                .catch(function(error) {alert(error)})
                                }>Resend</Button>
                            }
                             { authenticated &&
                                <Button variant="raised" color="secondary" style={AppBarItemStyle} onClick={() => auth.signOut()}>Log out</Button>
                            }
                        </Toolbar>
                    </AppBar>
                    { content }
                </div>
            </MuiThemeProvider>
         );
    }
}

export default withRouter(App);
