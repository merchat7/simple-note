import React, { Component } from 'react';
import {auth} from '../firebase';
import firebase from '../firebase';

import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});

class Profile extends Component {

    constructor(props) {
        super(props);
        const user = auth.currentUser;
        this.state = {
            name : user.displayName,
            email : user.email,
            newPassword : "",
            oldPassword : "",
            hasPassword : false,
            emailReadOnly : false
        };
        this.onSubmit = this.onSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentWillMount() {
        sessionStorage.setItem("ProfileVisited", true);
        auth.currentUser.providerData.forEach(profile => {
            if (profile.providerId === "password") this.setState({hasPassword: true});
            // TODO: Allow email to be changed even if sign-in from Google/Facebook
            if (profile.providerId === "google.com" || profile.providerId === "facebook.com") this.setState({emailReadOnly : true});
        });
    }

    onSubmit(event) {
        event.preventDefault();
        const {name, email, newPassword, oldPassword, hasPassword } = this.state;
        const user = auth.currentUser;
        const credential = firebase.auth.EmailAuthProvider.credential(
            user.email,
            oldPassword
        );
        // Always required password if user has a password
        if (hasPassword) {
            user.reauthenticateWithCredential(credential)
                .then(() => {
                    // FIXME: May not update all fields if email/password was changed?
                    user.updateProfile({displayName: name});
                    user.updateEmail(email).catch(error => alert(error));
                    if (newPassword.length !== 0) user.updatePassword(newPassword).catch(error => alert(error));
                    alert("Profile updated");
                }).catch(reauthError => {alert(reauthError);})
        }
        // For Facebook/Google account, allow "profile" to be changed
        else {
            user.updateProfile({displayName: name});
            alert("Profile update successful");
        }
        this.setState({newPassword: ""});
        this.setState({oldPassword: ""});
    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    render() {
        const {name, email, newPassword, oldPassword, hasPassword, emailReadOnly } = this.state;
        const classes = this.props.classes;
        return (
            <div>
                <Grid container>
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <h1>Profile</h1>
                            <form onSubmit={this.onSubmit} autoComplete="off">
                                <TextField
                                    id="name"
                                    label="Name"
                                    className={classes.textField}
                                    value={name}
                                    onChange={this.handleChange('name')}
                                    margin="normal"
                                    type="text"
                                />
                                <br />
                                <TextField
                                  id="email"
                                  label="Email"
                                  className={classes.textField}
                                  value={email}
                                  onChange={this.handleChange('email')}
                                  margin="normal"
                                  type="email"
                                  disabled={emailReadOnly}
                                />
                                <br />
                                {
                                    hasPassword &&
                                    <div>
                                        <TextField
                                            id="newPassword"
                                            label="New Password"
                                            className={classes.textField}
                                            value={newPassword}
                                            onChange={this.handleChange('newPassword')}
                                            margin="normal"
                                            type="password"
                                        />
                                        <br />
                                    </div>
                                }
                                {
                                    hasPassword &&
                                    <div>
                                        <TextField
                                            id="oldPassword"
                                            label="Old Password"
                                            className={classes.textField}
                                            value={oldPassword}
                                            onChange={this.handleChange('oldPassword')}
                                            margin="normal"
                                            type="password"
                                            required
                                        />
                                        <br />
                                    </div>
                                }
                                <Button variant="raised" color="primary" type="submit">Update</Button>
                            </form>
                        </Paper>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default withStyles(styles)(Profile);
