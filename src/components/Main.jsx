import React, { Component } from 'react';
import { auth, db } from '../firebase';
import { withStyles } from 'material-ui/styles';

import Button from 'material-ui/Button';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import Dialog, {
    DialogActions,
    DialogContent,
    DialogContentText,
} from 'material-ui/Dialog'
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';


const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
    list: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        maxWidth: 360,
        maxHeight: 200,
        overflow: 'auto',
    },

});

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
};

const grid = 8;

const getItemStyle = (isDragging, draggableStyle) => ({
    // some basic styles to make the items look a bit nicer
    userSelect: 'none',
    padding: grid * 2,
    margin: `0 0 ${grid}px 0`,

    // change background colour if dragging
    background: 'white',

    // styles we need to apply on draggables
    ...draggableStyle,
});

const getListStyle = isDraggingOver => ({
    background: 'white',
    padding: grid,
    width: 250,
});

class Main extends Component {

    constructor(props) {
        super(props);
        this.state = {
            notes : [],
            current : "",
            openDeleteDialog : false,
            selectedNoteIndex: null
        };
        this.addNote = this.addNote.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.onDragEnd = this.onDragEnd.bind(this);
    }

    componentWillMount() {
        if (sessionStorage.getItem("ProfileVisited")) {
            sessionStorage.removeItem("ProfileVisited");
            window.location.reload(true); // force refresh page1
        }
        const uid = auth.currentUser.uid;
        let notesRef = db.ref('notes/' + uid).orderByKey().limitToLast(100);
        notesRef.on('child_added', snapshot => {
            let note = { text: snapshot.val(), id: snapshot.key };
            this.setState({ notes: [note].concat(this.state.notes) });
        })
    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };


    addNote(e) {
        e.preventDefault();
        const uid = auth.currentUser.uid;
        db.ref('notes/' + uid).push(this.state.current);
        this.setState({ current : "" });
    }

    deleteNote = () => {
       const uid = auth.currentUser.uid;
       const noteid = this.state.notes[this.state.selectedNoteIndex].id;
       db.ref('notes/' + uid).child(noteid).remove();
       this.state.notes.splice(this.state.selectedNoteIndex, 1);
       this.handleClose();
    };

    handleOpen = (i) => {
        this.setState({openDeleteDialog: true});
        this.setState({selectedNoteIndex: i});
    };

    handleClose = () => {
        this.setState({openDeleteDialog: false});
        this.setState({selectedNoteIndex: null});
    };

    onDragEnd(result) {
        // dropped outside the list
        if (!result.destination) {
            return;
        }

        const items = reorder(
            this.state.notes,
            result.source.index,
            result.destination.index
        );
        this.setState({notes : items});
    }

    saveOrder= () => {
        const uid = auth.currentUser.uid;
        let notesRef = db.ref('notes/' + uid).orderByKey().limitToLast(100);
        let i = this.state.notes.length - 1;
        notesRef.once('value', snapshot => snapshot.forEach(child => {
            db.ref('notes/' + uid).child(child.key).set(this.state.notes[i].text);
            i -= 1;
        }));
        alert("Note saved");
    };

    render() {
        const classes = this.props.classes;
        return (
            <Grid container className={classes.container}>
                <Grid item xs={6}>
                    <Paper className={classes.paper}>
                        <p>Hello, { auth.currentUser.email }</p>
                        <DragDropContext onDragEnd={this.onDragEnd}>
                            <Droppable droppableId="droppable">
                                {(provided, snapshot) => (
                                    <div
                                        ref={provided.innerRef}
                                        style={getListStyle(snapshot.isDraggingOver)}
                                    >
                                        {this.state.notes.map((item, index) => (
                                            <Draggable key={item.id} draggableId={item.id} index={index}>
                                                {(provided, snapshot) => (
                                                    <div
                                                        ref={provided.innerRef}
                                                        {...provided.draggableProps}
                                                        {...provided.dragHandleProps}
                                                        style={getItemStyle(
                                                            snapshot.isDragging,
                                                            provided.draggableProps.style
                                                        )}
                                                    >
                                                        {item.text}
                                                        <IconButton aria-label="Delete" onClick={this.handleOpen.bind(this, index)}>
                                                            <DeleteIcon />
                                                        </IconButton>
                                                        <Dialog
                                                            open={this.state.openDeleteDialog}
                                                            onClose={this.handleClose}
                                                            aria-labelledby="alert-dialog-title"
                                                            aria-describedby="alert-dialog-description"
                                                        >
                                                            <DialogContent>
                                                                <DialogContentText id="alert-dialog-description">
                                                                    Delete the note?
                                                                </DialogContentText>
                                                            </DialogContent>
                                                            <DialogActions>
                                                                <Button onClick={this.handleClose} color="primary">
                                                                    Cancel
                                                                </Button>
                                                                <Button onClick={this.deleteNote} color="primary" autoFocus>
                                                                    Delete
                                                                </Button>
                                                            </DialogActions>
                                                        </Dialog>
                                                    </div>
                                                )}
                                            </Draggable>
                                        ))}
                                        {provided.placeholder}
                                    </div>
                                )}
                            </Droppable>
                        </DragDropContext>
                            <form onSubmit={this.addNote}>
                                <TextField
                                    id="note"
                                    label="Enter new note"
                                    className={classes.textField}
                                    value={this.state.current}
                                    onChange={this.handleChange('current')}
                                    margin="normal"
                                    />
                                <br />
                                <Button variant="raised" color="primary" type="submit">Add</Button>
                                <Button variant="raised" color="default" onClick={this.saveOrder}>Save ordering</Button>
                            </form>
                    </Paper>
                </Grid>
            </Grid>
        );
    }
}

export default withStyles(styles)(Main);
