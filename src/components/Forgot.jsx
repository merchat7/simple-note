import React, { Component } from 'react';
import { auth } from '../firebase';

import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});

class Forgot extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email : "",
        };
        this.onSubmit = this.onSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    onSubmit(event) {
        event.preventDefault();
        const { email } = this.state;
        auth.fetchProvidersForEmail(email)
            .then(providers => {
                let passwordFound = false;
                for (let i = 0; i < providers.length; i++) {
                    if (providers[i] === "password") {
                        auth.sendPasswordResetEmail(email)
                            .then(() => {
                                alert("Password reset email sent");
                                this.props.history.push('/login')
                            })
                            .catch(resetError => {alert(resetError)});
                        passwordFound = true;
                        break;
                    }
                }
                if (providers.length === 0) alert("Invalid email");
                else if (!passwordFound) alert("Accounts signed up through Facebook/Google cannot have password be reset");
            })
            .catch(inputError => {alert(inputError)});
    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    render() {
        const { email } = this.state;
        const classes = this.props.classes;
        return (
            <div>
                <Grid container>
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <h1>Password Reset</h1>
                            <form onSubmit={this.onSubmit} autoComplete="off">
                                <TextField
                                  id="email"
                                  label="Email"
                                  className={classes.textField}
                                  value={email}
                                  onChange={this.handleChange('email')}
                                  margin="normal"
                                  type="email"
                                />
                                <br />
                                <Button color="default" type="submit">Confirm</Button>
                            </form>
                        </Paper>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default withStyles(styles)(Forgot);
