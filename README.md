Link to app online: https://note-basic.firebaseapp.com

#Notes:
1. Only name can be changed in profile if signed in with Facebook/Google. Changing email & adding a password is not currently supported for such accounts
1. If a user made an account, then link it with Facebook/Google, password can be modified, and can login via either method
1. Password reset is not allowed for users who do not have a password (aka Google/Facebook account)